import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import slugify from 'slugify';
import { Article, NewsResponse } from '../models/responses/newsResponse';
import { ApiControllerService } from '../services/api-controller.service';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public searchQuery: string = '';
  public posts: Article[] = new Array();
  public isLoading: boolean = false;
  public onSearch: (query: string) => void;

  constructor(private apiController: ApiControllerService, private localStorageService: LocalStorageService) {
    this.onSearch = (query) => {
      this.searchQuery = query;

      this.isLoading = true;
      this.apiController.getNewsCardsFor(this.searchQuery).pipe(takeUntil(this.destroy$)).subscribe((data: Object) => {
        let newsResponse = <NewsResponse>data;

        newsResponse.articles.forEach(post => {
          post.publishedAt = post.publishedAt.substring(0, 10);
          post.slug = slugify(post.title);
        });

        let length = newsResponse.articles.length;

        if (length == 0) {
          this.posts = new Array();
        } else if (length < 24) {
          this.posts = newsResponse.articles.slice(0, length);
        } else {
          this.posts = newsResponse.articles.slice(0, 24);
        }

        this.localStorageService.saveNewsResponse(newsResponse);

        this.isLoading = false;
      });
    };
  }

  destroy$: Subject<boolean> = new Subject<boolean>();

  ngOnInit(): void {
  }
}
