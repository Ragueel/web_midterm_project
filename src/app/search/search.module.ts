import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SearchFormComponent } from './forms/search-form/search-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonObjectsModule } from '../common-objects/common-objects.module';

@NgModule({
  declarations: [
    SearchComponent,
    SearchFormComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ReactiveFormsModule,
    CommonObjectsModule,
  ]
})
export class SearchModule { }
