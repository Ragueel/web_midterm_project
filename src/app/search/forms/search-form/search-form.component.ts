import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  form = new FormGroup({
    query: new FormControl('', [Validators.required])
  })

  @Input() onSearch!: (searchQuery: string) => void;

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      if (this.onSearch != null) {
        this.onSearch(this.query?.value);
      }

      Object.keys(this.form.controls).forEach(key => {
        this.form.get(key)?.setErrors(null);
      });
    } else {
      Object.keys(this.form.controls).forEach(field => {
        const control = this.form.get(field);
        control?.markAsTouched({ onlySelf: true });
      });
    }
  }
  get query() {
    return this.form.get('query');
  }
}
