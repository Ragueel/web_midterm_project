import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribeToNewsFormComponent } from './subscribe-to-news-form.component';

describe('SubscribeToNewsFormComponent', () => {
  let component: SubscribeToNewsFormComponent;
  let fixture: ComponentFixture<SubscribeToNewsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscribeToNewsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribeToNewsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
