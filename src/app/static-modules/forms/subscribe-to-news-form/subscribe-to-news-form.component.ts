import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-subscribe-to-news-form',
  templateUrl: './subscribe-to-news-form.component.html',
  styleUrls: ['./subscribe-to-news-form.component.scss']
})
export class SubscribeToNewsFormComponent {

  public form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(4)])
  });

  get email() {
    return this.form.get('email');
  }

  onSubmit() {
    if (this.form.valid) {
      this.form.reset();

      Object.keys(this.form.controls).forEach(key => {
        this.form.get(key)?.setErrors(null);
      });
      alert('Thank you for subscribing');
    } else {
      Object.keys(this.form.controls).forEach(field => {
        const control = this.form.get(field);
        control?.markAsTouched({ onlySelf: true });
      });
    }
  }
}
