import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent {

  public form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(4)]),
    message: new FormControl('', [Validators.required, Validators.minLength(10)])
  });

  get email() {
    return this.form.get('email');
  }

  get message() {
    return this.form.get('message');
  }

  onSubmit() {
    if (this.form.valid) {
      this.form.reset();

      Object.keys(this.form.controls).forEach(key => {
        this.form.get(key)?.setErrors(null);
      });
      alert('We will soon be in touch with you!');
    } else {
      Object.keys(this.form.controls).forEach(field => {
        const control = this.form.get(field);
        control?.markAsTouched({ onlySelf: true });
      });
    }
  }
}
