import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticModulesComponent } from './static-modules.component';

describe('StaticModulesComponent', () => {
  let component: StaticModulesComponent;
  let fixture: ComponentFixture<StaticModulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaticModulesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
