import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticModulesRoutingModule } from './static-modules-routing.module';
import { StaticModulesComponent } from './static-modules.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { JoinPageComponent } from './join-page/join-page.component';
import { SubscribeToNewsFormComponent } from './forms/subscribe-to-news-form/subscribe-to-news-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactFormComponent } from './forms/contact-form/contact-form.component';


@NgModule({
  declarations: [
    StaticModulesComponent,
    AboutPageComponent,
    JoinPageComponent,
    SubscribeToNewsFormComponent,
    ContactFormComponent
  ],
  imports: [
    CommonModule,
    StaticModulesRoutingModule,
    ReactiveFormsModule
  ]
})
export class StaticModulesModule { }
