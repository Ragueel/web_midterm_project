import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StaticModulesComponent } from './static-modules.component';

import { AboutPageComponent } from './about-page/about-page.component';
import { JoinPageComponent } from './join-page/join-page.component';

const routes: Routes = [
  { path: '', component: StaticModulesComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'join', component: JoinPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticModulesRoutingModule { }