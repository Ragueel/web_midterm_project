import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'midterm';

  public isMenuOpen = false;

  onMenuClick() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  onMobileRouteClick() {
    this.isMenuOpen = false;
  }
}
