import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullViewPageComponent } from './full-view-page/full-view-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { annotation, menu, HeroIconModule } from 'ng-heroicon';
import { UserInfoPageComponent } from './user-info-page/user-info-page.component';
import { LogInterceptor } from './interceptor/logInterceptor';
import { LocalStorageService } from './services/local-storage.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonObjectsModule } from './common-objects/common-objects.module';

@NgModule({
  declarations: [
    AppComponent,
    FullViewPageComponent,
    MainPageComponent,
    PageNotFoundComponent,
    UserInfoPageComponent,
  ],
  imports: [
    HeroIconModule.withIcons({
      annotation,
      menu
    }),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonObjectsModule,
  ],
  providers: [
    LocalStorageService,
    { provide: HTTP_INTERCEPTORS, useClass: LogInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
