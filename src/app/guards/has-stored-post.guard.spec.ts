import { TestBed } from '@angular/core/testing';

import { HasStoredPostGuard } from './has-stored-post.guard';

describe('HasStoredPostGuard', () => {
  let guard: HasStoredPostGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasStoredPostGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
