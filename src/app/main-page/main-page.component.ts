import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Article, NewsResponse } from '../models/responses/newsResponse';
import { ApiControllerService } from '../services/api-controller.service';
import slugify from 'slugify';
import { LocalStorageService } from '../services/local-storage.service';
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, OnDestroy {

  public posts: string[] = ['', '', '', ''];
  public featuredPosts: Article[] = new Array();
  public latestPosts: Article[] = new Array();
  public otherPosts: Article[] = new Array();
  public recommendedPosts: Article[] = new Array();
  public quickReads: Article[] = new Array();

  public featuredPost: Article = <Article>new Object();

  destroy$: Subject<boolean> = new Subject<boolean>();

  public isLoading: boolean = true;


  constructor(private apiControllerService: ApiControllerService, private localStorageService: LocalStorageService) { }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.apiControllerService.getNewsCardsFor('SpaceX').pipe(takeUntil(this.destroy$)).subscribe((data: Object) => {
      let newsResponse = <NewsResponse>data;

      newsResponse.articles.forEach(post => {
        post.publishedAt = post.publishedAt.substring(0, 10);
        post.slug = slugify(post.title);
      });

      this.featuredPosts = newsResponse.articles.slice(0, 2);
      this.latestPosts = newsResponse.articles.slice(2, 8);
      this.recommendedPosts = newsResponse.articles.slice(8, 12);
      this.otherPosts = newsResponse.articles.slice(12, 16);
      this.featuredPost = newsResponse.articles[17];
      this.quickReads = newsResponse.articles.slice(18, 22);

      this.localStorageService.saveNewsResponse(newsResponse);

      this.isLoading = false;
    });
  }

  arrayOf(num: number) {
    return Array(num).fill(0);
  }
}
