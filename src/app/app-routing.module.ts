import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { FullViewPageComponent } from './full-view-page/full-view-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HasStoredPostGuard } from './guards/has-stored-post.guard';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'post/:title', component: FullViewPageComponent, canActivate: [HasStoredPostGuard] },
  { path: 'staticModules', loadChildren: () => import('./static-modules/static-modules.module').then(m => m.StaticModulesModule) },
  { path: 'search', loadChildren: () => import('./search/search.module').then(m => m.SearchModule) },
  { path: '**', loadChildren: () => import('./errors/errors.module').then(m => m.ErrorsModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
