import { Injectable } from '@angular/core';
import { PostComment } from '../models/post-comment.model';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class PostRetrievalService {

  constructor(private localStorageService: LocalStorageService) { }

  public getArticleSlice(start: number, end: number) {
    let response = this.localStorageService.getResponse();

    return response.articles.slice(start, end);
  }

  public getArticle(slug: string) {
    let response = this.localStorageService.getResponse();

    return response.articles.find((a) => a.slug == slug);
  }

  public getComments(slug: string) {
    let comments = this.localStorageService.getComments(slug);

    return comments;
  }

  public addComment(postSlug: string, postComment: PostComment) {
    let comments = this.localStorageService.getComments(postSlug);
    comments.push(postComment);

    this.localStorageService.setComments(postSlug, comments);

    return comments;
  }

  public addSubComment(commentId: string, postComment: PostComment) {
    let comments = this.localStorageService.getSubcomments(commentId);
    comments.push(postComment);

    this.localStorageService.setSubComments(commentId, comments);

    return comments;
  }


  public getSubComments(commentId: string) {
    return this.localStorageService.getSubcomments(commentId);
  }
}
