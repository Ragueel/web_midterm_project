import { Injectable } from '@angular/core';
import { PostComment } from '../models/post-comment.model';
import { NewsResponse } from '../models/responses/newsResponse';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  storageItemName: string = 'posts';

  constructor() { }

  public getResponse() {
    const jsonString: string = localStorage.getItem(this.storageItemName) || '';

    const responseData = <NewsResponse>JSON.parse(jsonString);

    return responseData;
  }

  public saveNewsResponse(response: NewsResponse) {
    localStorage.setItem(this.storageItemName, JSON.stringify(response));
  }

  public postsAreStored() {
    return localStorage.getItem('posts') != null;
  }

  private getCommentKey(slug: string) {
    return `comments_${slug}`;
  }

  private getSubCommentKey(commentId: string) {
    return `subComments_${commentId}`;
  }

  public getComments(postSlug: string) {
    const commentString = localStorage.getItem(this.getCommentKey(postSlug)) || '[]';

    const comments = <PostComment[]>JSON.parse(commentString);
    return comments;
  }

  public getSubcomments(commentId: string) {
    const subComments = localStorage.getItem(this.getSubCommentKey(commentId)) || '[]';
    const comments = <PostComment[]>JSON.parse(subComments);

    return comments;
  }

  public setComments(postSlug: string, comments: PostComment[]) {
    localStorage.setItem(this.getCommentKey(postSlug), JSON.stringify(comments));
  }

  public setSubComments(commentId: string, comments: PostComment[]) {
    localStorage.setItem(this.getSubCommentKey(commentId), JSON.stringify(comments));
  }
}