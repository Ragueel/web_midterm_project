import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiControllerService {
  baseUrl: string = 'https://newsapi.org/v2';

  constructor(private httpClient: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    let errorMessage = 'Some error';

    if (error.error instanceof ErrorEvent) {
      errorMessage = `Got error: ${error.error.message}`;
    } else {
      errorMessage = `Error code: ${error.status}\nMessage: ${error.message}`;
    }

    console.log(errorMessage);
    return throwError(() => { new Error(errorMessage) });
  }

  public getNewsCardsFor(query: string) {
    let params = new HttpParams();
    params = params.append('apiKey', environment.newsApiKey);
    params = params.append('q', query);
    params = params.append('from', '2021-12-20');
    params = params.append('sortBy', 'popularity');
    params = params.append('pageSize', 100);
    params = params.append('language', 'en');

    const options = { params: params };

    return this.httpClient.get(`${this.baseUrl}/everything`, options).pipe(catchError(this.handleError));
  }
}
