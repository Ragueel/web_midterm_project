import { TestBed } from '@angular/core/testing';

import { PostRetrievalService } from './post-retrieval.service';

describe('PostRetrievalServiceService', () => {
  let service: PostRetrievalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostRetrievalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
