import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullViewPageComponent } from './full-view-page.component';

describe('FullViewPageComponent', () => {
  let component: FullViewPageComponent;
  let fixture: ComponentFixture<FullViewPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FullViewPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullViewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
