import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostComment } from '../models/post-comment.model';
import { Article } from '../models/responses/newsResponse';
import { LocalStorageService } from '../services/local-storage.service';
import { PostRetrievalService } from '../services/post-retrieval.service';

@Component({
  selector: 'app-full-view-page',
  templateUrl: './full-view-page.component.html',
  styleUrls: ['./full-view-page.component.scss']
})
export class FullViewPageComponent implements OnInit {
  public article: Article = new Object as Article;
  public quickReads: Article[] = new Array();
  public comments: PostComment[] = new Array();
  public hasFoundArticle = true;
  public isLoadingArticle = true;
  public slug: string = '';
  public onCommentCreated: (comment: PostComment) => void;

  constructor(private postRetrievalService: PostRetrievalService, private route: ActivatedRoute, private localStorageService: LocalStorageService) {
    this.onCommentCreated = (comment) => {
      this.comments = this.postRetrievalService.addComment(this.slug, comment);
    };
  }

  ngOnInit(): void {
    this.isLoadingArticle = true;
    this.route.params.subscribe(param => {
      this.slug = param['title'];
      let article = this.postRetrievalService.getArticle(this.slug);
      this.comments = this.postRetrievalService.getComments(this.slug);

      if (article == null) {
        this.hasFoundArticle = false;
        return;
      }
      this.isLoadingArticle = false;
      this.article = article;

      let randomNumber = Math.random() * this.localStorageService.getResponse().articles.length;

      this.quickReads = this.postRetrievalService.getArticleSlice(randomNumber, randomNumber + 4);
    });
  }

}
