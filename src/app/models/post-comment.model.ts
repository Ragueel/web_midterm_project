export class PostComment {
    public email: string;
    public username: string;
    public comment: string;
    public creationDate: string;
    public id: string;

    constructor(id: string, email: string, username: string, comment: string, creationDate: string) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.comment = comment;
        this.creationDate = creationDate;
    }
}
