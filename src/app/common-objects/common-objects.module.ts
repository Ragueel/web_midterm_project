import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostCardComponent } from './sharedComponents/post-card/post-card.component';
import { PostCardTitledComponent } from './sharedComponents/post-card-titled/post-card-titled.component';
import { PostCardMinifiedComponent } from './sharedComponents/post-card-minified/post-card-minified.component';
import { BaseCardComponent } from './sharedComponents/base-card/base-card.component';
import { AppRoutingModule } from '../app-routing.module';
import { QuickReadComponent } from './sharedComponents/quick-read/quick-read.component';
import { AddCommentComponent } from './forms/add-comment/add-comment.component';
import { PostCommentComponent } from './sharedComponents/post-comment/post-comment.component';
import { RouterModule } from '@angular/router';
import { SimpleCardLoaderComponent } from './sharedComponents/skeletons/simple-card-loader/simple-card-loader.component';
import { TitledCardLoaderComponent } from './sharedComponents/skeletons/titled-card-loader/titled-card-loader.component';
import { MinifiedCardLoaderComponent } from './sharedComponents/skeletons/minified-card-loader/minified-card-loader.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PostCardComponent,
    PostCardTitledComponent,
    PostCardMinifiedComponent,
    BaseCardComponent,
    QuickReadComponent,
    PostCommentComponent,
    SimpleCardLoaderComponent,
    TitledCardLoaderComponent,
    MinifiedCardLoaderComponent,
    AddCommentComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  exports: [
    PostCardComponent,
    PostCardTitledComponent,
    PostCardMinifiedComponent,
    BaseCardComponent,
    QuickReadComponent,
    PostCommentComponent,
    SimpleCardLoaderComponent,
    TitledCardLoaderComponent,
    MinifiedCardLoaderComponent,
    AddCommentComponent,
  ]
})
export class CommonObjectsModule { }
