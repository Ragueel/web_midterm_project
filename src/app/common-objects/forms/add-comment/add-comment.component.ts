import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PostComment } from 'src/app/models/post-comment.model';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.scss']
})
export class AddCommentComponent {
  form = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(4), Validators.nullValidator]),
    email: new FormControl('', [Validators.required, Validators.minLength(6), Validators.email, Validators.nullValidator]),
    comment: new FormControl('', [Validators.required, Validators.minLength(10), Validators.nullValidator])
  });

  @Input() onCommentCreated!: (comment: PostComment) => void;

  makeid(length: number): string {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }

  onSubmit() {
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      if (this.onCommentCreated != null) {
        const dateNow = new Date();

        let postComment = new PostComment(this.makeid(12), this.email?.value, this.username?.value, this.comment?.value, dateNow.toLocaleString().split('/').join('-'));

        this.onCommentCreated(postComment);
      }

      this.form.reset();

      Object.keys(this.form.controls).forEach(key => {
        this.form.get(key)?.setErrors(null);
        this.form.get(key)?.setValue(null);
      });
    } else {
      Object.keys(this.form.controls).forEach(field => {
        const control = this.form.get(field);
        control?.markAsTouched({ onlySelf: true });
      });
    }
  }

  get username() {
    return this.form.get('username');
  }

  get email() {
    return this.form.get('email');
  }

  get comment() {
    return this.form.get('comment');
  }
}
