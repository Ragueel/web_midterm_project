import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleCardLoaderComponent } from './simple-card-loader.component';

describe('SimpleCardLoaderComponent', () => {
  let component: SimpleCardLoaderComponent;
  let fixture: ComponentFixture<SimpleCardLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleCardLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleCardLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
