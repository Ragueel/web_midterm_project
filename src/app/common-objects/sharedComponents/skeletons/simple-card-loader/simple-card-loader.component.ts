import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-card-loader',
  templateUrl: './simple-card-loader.component.html',
  styleUrls: ['./simple-card-loader.component.scss']
})
export class SimpleCardLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
