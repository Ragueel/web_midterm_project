import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinifiedCardLoaderComponent } from './minified-card-loader.component';

describe('MinifiedCardLoaderComponent', () => {
  let component: MinifiedCardLoaderComponent;
  let fixture: ComponentFixture<MinifiedCardLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinifiedCardLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MinifiedCardLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
