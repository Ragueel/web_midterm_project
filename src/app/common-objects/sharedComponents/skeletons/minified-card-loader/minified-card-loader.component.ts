import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-minified-card-loader',
  templateUrl: './minified-card-loader.component.html',
  styleUrls: ['./minified-card-loader.component.scss']
})
export class MinifiedCardLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
