import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitledCardLoaderComponent } from './titled-card-loader.component';

describe('TitledCardLoaderComponent', () => {
  let component: TitledCardLoaderComponent;
  let fixture: ComponentFixture<TitledCardLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TitledCardLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TitledCardLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
