import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-titled-card-loader',
  templateUrl: './titled-card-loader.component.html',
  styleUrls: ['./titled-card-loader.component.scss']
})
export class TitledCardLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
