import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCardMinifiedComponent } from './post-card-minified.component';

describe('PostCardMinifiedComponent', () => {
  let component: PostCardMinifiedComponent;
  let fixture: ComponentFixture<PostCardMinifiedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostCardMinifiedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCardMinifiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
