import { Component, Input, OnInit } from '@angular/core';

import { BaseCardComponent } from '../base-card/base-card.component';
@Component({
  selector: 'app-post-card-minified',
  templateUrl: './post-card-minified.component.html',
  styleUrls: ['./post-card-minified.component.scss']
})
export class PostCardMinifiedComponent extends BaseCardComponent {
}
