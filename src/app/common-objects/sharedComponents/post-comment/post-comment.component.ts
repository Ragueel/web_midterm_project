import { Component, Input, OnInit } from '@angular/core';
import { PostComment } from 'src/app/models/post-comment.model';
import { PostRetrievalService } from 'src/app/services/post-retrieval.service';

@Component({
  selector: 'app-post-comment',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.scss']
})
export class PostCommentComponent implements OnInit {

  @Input() username: string = '';
  @Input() date: string = '';
  @Input() comment: string = '';
  @Input() commentId: string = '';

  public subComments: PostComment[] = new Array();
  public onCommentCreated: (comment: PostComment) => void;

  public isLeavingReply = false;

  constructor(private postRetrievalService: PostRetrievalService, private postRetreivalService: PostRetrievalService) {
    this.onCommentCreated = (comment: PostComment) => {
      this.subComments.push(comment);
      this.postRetreivalService.addSubComment(this.commentId, comment);
      this.isLeavingReply = false;
    }
  }

  ngOnInit(): void {
    this.subComments = this.postRetrievalService.getSubComments(this.commentId);
  }

  onLeaveReplyClick() {
    this.isLeavingReply = true;
  }

}
