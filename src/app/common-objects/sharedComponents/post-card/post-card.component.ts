import { Component, Input, OnInit } from '@angular/core';

import { BaseCardComponent } from '../base-card/base-card.component';
@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent extends BaseCardComponent {

}
