import { Component, Input, OnInit } from '@angular/core';
import { BaseCardComponent } from '../base-card/base-card.component';
@Component({
  selector: 'app-post-card-titled',
  templateUrl: './post-card-titled.component.html',
  styleUrls: ['./post-card-titled.component.scss']
})
export class PostCardTitledComponent extends BaseCardComponent {

}
