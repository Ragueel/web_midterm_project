import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCardTitledComponent } from './post-card-titled.component';

describe('PostCardTitledComponent', () => {
  let component: PostCardTitledComponent;
  let fixture: ComponentFixture<PostCardTitledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostCardTitledComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCardTitledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
