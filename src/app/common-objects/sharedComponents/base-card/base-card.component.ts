import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-card',
  templateUrl: './base-card.component.html',
  styleUrls: ['./base-card.component.scss']
})
export class BaseCardComponent implements OnInit {

  @Input() imageUrl: string = '';
  @Input() title: string = '';
  @Input() date: string = '';
  @Input() postSlug: string = 'test';
  @Input() category: string = 'Test';
  postLink: string = '';

  constructor() { }

  ngOnInit(): void {
    this.postLink = `/post/${this.postSlug}`;
  }

}
