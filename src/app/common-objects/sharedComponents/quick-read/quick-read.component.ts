import { Component, Input } from '@angular/core';
import { BaseCardComponent } from '../base-card/base-card.component';

@Component({
  selector: 'app-quick-read',
  templateUrl: './quick-read.component.html',
  styleUrls: ['./quick-read.component.scss']
})
export class QuickReadComponent extends BaseCardComponent {
  @Input() shortDescription: string = '';
}
