const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.coolGray,
        blue: colors.lightBlue,
        red: colors.rose900,
        pink: colors.fuchsia,
      },
    },
    
  },
  variants: {
    extend: {},
  },
  plugins: [
    
  ],
}
